const express = require('express'); // Importing express module
  
const app = express(); // Creating an express object
  
const port = 8000;  // Setting an port for this application


  
// Starting server using listen function
app.listen(port, function (err) {
   if(err){
       console.log("Error while starting server");
   }
   else{
       console.log("Server has been started at "+port);
   }
})
app.get('/api/website', function (req, res) {
    const metascraper = require('metascraper')([
        require('metascraper-author')(),
        require('metascraper-date')(),
        require('metascraper-description')(),
        require('metascraper-image')(),
        require('metascraper-logo')(),
        require('metascraper-clearbit')(),
        require('metascraper-publisher')(),
        require('metascraper-title')(),
        require('metascraper-url')()
      ])
      
    
      const got = require('got')
      
      const targetUrl = req.query['url']
      ;(async () => {
        const { body: html, url } = await got(targetUrl)
        const metadata = await metascraper({ html, url })
        res.send(metadata)
      })()
  })